describe('HTTP routes', () => {
    const supertest = require('supertest');
    const app = require('../api/app');

    describe('/checkpassword', () => {
        describe('POST /checkpassword', () => {
            describe('When the request have an empty body', () => {
                it('The API should return an error related with a bad request (400 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .expect(400);
                });
            });

            describe('When the request have an extremely large password', () => {
                it('The API should return an error related with a bad request (400 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .send({password: '123456789123456789123456789123456789123456789'})
                        .expect(400);
                });
            });
            
            describe('When the request have a not-a-string password', () => {
                it('The API should return an error related with a bad request (400 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .send({password: 1})
                        .expect(400);
                });
            });
            
            describe('When the request has additional fields', () => {
                it('The API should return an error related with a bad request (400 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .send({
                            password: 'password',
                            anotherField: 'randomValue'
                        })
                        .expect(400);
                });
            });

            describe('When the request is valid', () => {
                it('The API should return a correct response (200 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .send({
                            password: 'password'                        
                        })
                        .expect(200);
                });

                it('The API should return a correct response (200 status code)', () => {
                    return supertest(app)
                        .post('/checkpassword')
                        .send({
                            password: 'password'                        
                        })
                        .expect(200);
                });
            });
        });

        describe('GET /checkpassword', () => {
            it('should be disabled', () => {
                return supertest(app)
                        .get('/password')
                        .expect(404);
            });
        });

        describe('DELETE /checkpassword', () => {
            it('should be disabled', () => {
                return supertest(app)
                        .delete('/password')
                        .expect(404);
            });
        });

        describe('PUT /checkpassword', () => {
            it('should be disabled', () => {
                return supertest(app)
                        .put('/password')
                        .expect(404);
            });
        });

        describe('PATCH /checkpassword', () => {
            it('should be disabled', () => {
                return supertest(app)
                        .patch('/password')
                        .expect(404);
            });
        });        
    });
});

describe('Internal methods', () => {
    const {checkPassword} = require('../api/domain');

    describe('Domain', () => {
        describe('Check Password', () => {
            describe('When a most used password is sent', () => {
                const TYPICAL_PASSWORD = '!!!lars!!!';

                it('should return a true value', async () => {
                    const response = await checkPassword(TYPICAL_PASSWORD);

                    expect(response).toEqual(true);
                });

                it('should stop the process before reach the end of the file (EOF)', async () => {
                    const Events = require('events');
                    spyOn(Events, 'once');                    
                    await checkPassword(TYPICAL_PASSWORD);

                    expect(Events.once).not.toHaveBeenCalled();
                });
            });

            describe('When a NOT most used password is sent', () => {
                it('should return a false value', async () => {
                    const TYPICAL_PASSWORD = 'This_is_a_weird_password';
                    const response = await checkPassword(TYPICAL_PASSWORD);

                    expect(response).toEqual(false);
                });
            });

            describe('Sending as parameter a NOT most used password', () => {
                it('should return a false value', async () => {
                    const TYPICAL_PASSWORD = 'This_is_a_weird_password';
                    const response = await checkPassword(TYPICAL_PASSWORD);

                    expect(response).toEqual(false);
                });
            });
        });
    });
});