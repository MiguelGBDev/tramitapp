# TRAMIT APP

This proof is made by Miguel Gomez for Tramit App technical proof.

## SET UP PROJECT

### Requirements
For run this project its required to have a **UNIX-based** distribution operative system (Ubuntu, macOS...).

- Install node.js
- Install npm

You can use this short command to install both:
- sudo apt install nodejs npm

Once this programs has been installed, its required to install internal dependencies, for this, just type in terminal placed at this same folder:
- npm install

If all runned well, you are ready to use this project correctly.

You have three options:
- Run the test suite directly: **npm run test**: 
    This option use jasmine for run the tests, when it finished, the program is closed and the results will be prompted in terminal.
- Run the server as in a production environment: **npm start**
    This way starts a server for listen requests
- Run the server for development: **npm run dev**
    This option starts a server but this will be restart when code is changed and saved, so you could use it for checks things in code internally.

## FIRST PART
First part consists in make an API with and enpoint where a password will be searched in a large file with the most tipically passwords. If the password sent is found the endpoint will return a **True** response, if not, **False**.

The implemented algorithm is really fast and stop searching if the password was found. It was made with streams, as requested, and using anm useful library called readLine.

For faster checks, a test suite implemented with jasmine platform is setted.

The test suite is divided in two parts:
- HTTP Requests to the implemented API.
- Internal method with the search algorithm.

The tests suite uses supertest library for HTTP request to the API.
Also, there are test related with the internal algorithm, forgotting HTTP protocol or requests, focusing in the search method.

Just type **npm run test** and jasmine will show the results.

To see the implemented test suite, please check **/spec/index.spec.js**.

Also, you could use external tools as Postman, i.e.
Just redirect your HTTP requests to **localhost:6500** and the endpoint enabled: **"/checkpassword"**

## SECOND PART
The second part is sort a CSV file by population in a descent way.

As the was not requested, I decided to implement it using **linux bash commands**, but it could be made through **node.js** using **npm** libraries as **'csv'** or **'csvtojson'**

For run this bash script, just type **"npm run sort"** and the script **"sort_population.sh"** placed at **"/scripts"** folder will be executed, generating a CSV called **"sorted_population.csv"** in the same folder, with the data from **"countries_population.csv"** given in the proof request.

## ADDITIONS
Aditionally a dockerfile is included in the project for easy server deployment.

For run this project inside a container, its required to install Docker.

**Further reading for install Docker**:
https://www.hostinger.es/tutoriales/como-instalar-y-usar-docker-en-ubuntu/

Once, docker is installed and running, its time to create an image from our project, through a **DockerFile** script.

The command required is:
**docker build --tag tramitapp_image:1.0.0 .**

Once the image is created, we could create a container from this image:
**docker run -d -p 6500:6500 tramitapp_image:1.0.0**

With this instruction we create a container and it starts to run automatically. So, now, we could use the server directly at the same way explained before instead ofinstall any dependencies.

If you want to stop the server containerized. Just type: 
docker stop $(docker ps -a -q)

**CAUTION**: This instruction will stop all containers in your machine, not only this project.

**Miguel Gómez, 20 May 2020**