#!/bin/bash

awk -F, '{print $1$2$3$4}' OFS=',' countries_population.csv > sorted_population.csv && sort -r -t";" -g -k2 -o sorted_population.csv sorted_population.csv
