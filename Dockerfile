#Initial image from docker hub
FROM node:10

#App directory definition
WORKDIR /usr/src/app

#Copy dependencies within container
COPY package.json ./

#Install dependencies from package.json
RUN npm install

#Copy the rest of the project
#With this structure Docker's cache help to speed up container creation
COPY . .

#Expose the application port
EXPOSE 6500

#Command for run the application
CMD ["npm", "start"]