const { once } = require('events');
const { createReadStream } = require('fs');
const { createInterface } = require('readline');
const {join} = require('path');

const passwordsFile = join(__dirname, '../persistence/xato-net-10-million-passwords.txt');

/**
 * CheckPassword: Search a password in a static file.
 * 
 * The stream reads a file line by line, searching the string given as parameter.
 * If the stream found the end of file (EOF), it means password is not found, and return result to user. 
 * 
 * If the password was found, the stream stops reading and the result is sent to user.
 * In order to stop the stream read, its required to close the readable stream and disable events. 
 * 
 * ReadLine library is used in order to check strings instead bytes chunks. 
 * Check bytes chunks is faster, but harder to manage. 
 * Also, its required to compare the data received with a given password,
 * so all chunk must be converted as string for this comparation, so the performance increase will be lost.
 * This is the reason for use readline finally.
 * 
 * @async
 * @param {String} password - Password string that will be searched
 * @returns {Promise.<Boolean>} - The result of the password's search within the file.
 */
const _checkPassword = async (password) => {
    let found = false;
    
    const reader = createReadStream(passwordsFile);
    const rl = createInterface({
        input: reader,
        crlfDelay: Infinity
    });

    rl.on('line', (line) => {
        if (line === password) {
            found = true;
            
            rl.close();
            rl.removeAllListeners();
        }
    });

    await once(rl, 'close');

    return found;
};

module.exports = {
    checkPassword: _checkPassword
}