const {validateFormat} = require('../utils/validationService');

/**
 * ValidateRequest: compare request body with an AJV schema.
 * 
 * If the format is valid according to the schema, the request will be processed.
 * If not, the respective errors are collected 
 * and returned rejecting the request with a bad request error code (400).
 *   
 * @param {Object} req - Request object to the API
 * @param {Object} res - Predefined response made by express
 * @param {Function} next - Callback to the next function if the validation is approved
 */
const _validateRequest = (req, res, next) => {
    try {
        const input = req.body;            
        const {pathname} = req._parsedUrl;
        const route = pathname.substring(1);
                   
        const validation = validateFormat(route, input);
        if (validation.isValid) {
            next();
        } else {
            const error = new Error('Bad Request');
            error.errors = validation.errors;
            throw error;
        }
    } catch (err) {
        res.status(400).json({ error: err.message, errors: err.errors });
    }
}

module.exports = {
    validateRequest: _validateRequest
};