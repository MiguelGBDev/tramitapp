const Ajv = require('ajv');

const schemas = require('./ajvSchemas');

const _validateFormat = (route, input) => {
    const schema = schemas[route];
    
    const validator = new Ajv({allErrors: true})
                            .compile(schema);

    const isValid = validator(input);    

    return {isValid, errors: validator.errors};
}
 
module.exports = {
    validateFormat: _validateFormat
};