/**
 * This program was created by Miguel Ángel Gómez Baena. All rights reserved.
 */
const port = process.env.PORT || 6500;
const app = require('./app');

// Start server
app.listen(port, () => {
    console.log('API running on http://localhost:6500');
});