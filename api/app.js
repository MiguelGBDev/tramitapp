const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require ('body-parser');

//methods which compute endpoint requests
const routes = require('./routes');

//middlewares definitions for requests checks
const {validateRequest} = require('./middlewares');

// Express configuration
app.use(router);
app.use(bodyParser.json());
app.use(bodyParser.text({ limit: '256mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }));

//  Definition of the app's routes
app.post('/checkpassword', validateRequest, routes.checkPassword);

module.exports = app;