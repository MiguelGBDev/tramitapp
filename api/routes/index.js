const domain = require('../domain');

const _checkPassword = async (req, res) => {
    const {password} = req.body;
    
    try {
        const response = await domain.checkPassword(password);
        res.status(200).send(response);
    } catch (err) {                
        res.status(500).json({ message: 'Internal server error' });
    }
};

module.exports = {    
    checkPassword: _checkPassword
}